#! /usr/bin/env python

from distutils.core import setup

setup(
    name = 'python-logging-extra',
    version = '0.1',
    packages=['loggingx'],
    # Metadata
    author = 'ARCO Research Group',
    author_email = 'sysadm(at)arco.esi.uclm.es',
    description = 'Generic utilities for the Python loggin facility',
    license = 'GPLv3',
    url = ' http://arco.esi.uclm.es'
)
